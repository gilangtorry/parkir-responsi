package dev.gilang.parkom.api;

import com.google.gson.annotations.SerializedName;


public class ApiResponse<T> {
    public int status;
    public String message;
    @SerializedName("data") T data;

    public T getResponse()
    {
        return data;
    }

}

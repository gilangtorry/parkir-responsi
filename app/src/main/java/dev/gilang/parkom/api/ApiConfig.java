package dev.gilang.parkom.api;

import com.google.gson.GsonBuilder;

import java.util.concurrent.TimeUnit;

import dev.gilang.parkom.BuildConfig;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

public class ApiConfig {

    private static Retrofit retrofit;

    public static Retrofit getRetrofit()
    {
        if (retrofit == null)
        {
            GsonBuilder builder = new GsonBuilder();
            builder.setLenient();
            retrofit = new Retrofit.Builder()
                    .baseUrl(ApiEndpoints.BASE_URL)
                    .client(getOkHttpClient())
                    .addConverterFactory(GsonConverterFactory.create(builder.create()))
                    .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                    .build();
        }

        return retrofit;
    }

    private static OkHttpClient getOkHttpClient()
    {
        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        if(BuildConfig.DEBUG)logging.setLevel(HttpLoggingInterceptor.Level.BODY);
        else logging.setLevel(HttpLoggingInterceptor.Level.NONE);

        return new OkHttpClient.Builder()
                .connectTimeout(30, TimeUnit.SECONDS)
                .writeTimeout(30, TimeUnit.SECONDS)
                .readTimeout(30, TimeUnit.SECONDS)
                .retryOnConnectionFailure(true)
                .addInterceptor(logging)
                .cache(null)
                .build();
    }
}

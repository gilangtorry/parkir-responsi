package dev.gilang.parkom.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import dev.gilang.parkom.models.ParkirModel;
import dev.gilang.parkom.R;

public class ParkirAdapter extends RecyclerView.Adapter<ParkirAdapter.ItemHolder>{
    private Context context;
    private List<ParkirModel> item = new ArrayList<>();
    private OnItemClickListener onItemClickListener;

    public ParkirAdapter(Context context){
        this.context = context;
    }
    @NonNull
    @Override
    public ParkirAdapter.ItemHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_parkir, viewGroup, false);
        ParkirAdapter.ItemHolder item = new ParkirAdapter.ItemHolder(view);
        return item;
    }

    @Override
    public void onBindViewHolder(@NonNull ParkirAdapter.ItemHolder itemHolder, int i) {
        ParkirModel data = item.get(i);
        itemHolder.icon.setImageResource(data.jenis.equals("Motor") ? R.drawable.motor:R.drawable.ic_car);
        itemHolder.no_urut.setText(data.no_parkir);
        itemHolder.nopol.setText(data.nopol);
        itemHolder.tanggal.setText(data.tgl_parkir);
        itemHolder.root.setOnClickListener(v -> {
            if(onItemClickListener!=null)
                onItemClickListener.onItemClick(v, data, i);
        });
    }

    public void setData(List<ParkirModel> data){
        item.clear();
        item.addAll(data);
        notifyDataSetChanged();
    }

    public void clearData(){
        item.clear();
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return item.size();
    }

    public class ItemHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.root) View root;
        @BindView(R.id.img) ImageView icon;
        @BindView(R.id.tvNopol) TextView nopol;
        @BindView(R.id.tvUrut) TextView no_urut;
        @BindView(R.id.tvTgl) TextView tanggal;
        public ItemHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    public interface OnItemClickListener {
        void onItemClick(View view, ParkirModel obj, int position);
    }

    public void setOnItemClickListener(final OnItemClickListener mItemClickListener) {
        this.onItemClickListener = mItemClickListener;
    }
}
